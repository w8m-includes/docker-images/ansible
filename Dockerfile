FROM python:3.10-alpine

ENV ANSIBLE_VERSION=7.4.0
ENV ANSIBLELINT_VERSION=6.13.1

WORKDIR /opt

COPY ansible.cfg entrypoint.sh .

RUN apk add --no-cache \
    git openssh-client rsync gcc musl-dev

RUN pip3 install --no-cache-dir \
    ansible~=${ANSIBLE_VERSION} \
    ansible-lint~=${ANSIBLELINT_VERSION} \
    && python3 -m pip cache purge

RUN mkdir ~/.ssh && \
    ssh-keyscan -t rsa gitlab.com >> ~/.ssh/known_hosts

ENTRYPOINT ["/bin/bash", "./entrypoint.sh"]